$(this).ready(function () {
  $('.slick').slick({
    autoplay: true,
    vertical: true,
    arrows: false,
    dots: false,
    pauseOnHover:true,
    draggable: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1

  });
  var maxHeight = -1;
  $('.slick-slide').each(function () {
    if ($(this).height() > maxHeight) {
      maxHeight = $(this).height();
    }
  });
  $('.slick-slide').each(function () {
    if ($(this).height() < maxHeight) {
      $(this).css('margin', Math.ceil((maxHeight - $(this).height()) / 2) + 'px 0');
    }
  });
});

