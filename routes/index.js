var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', imgsrc: req.app.get("img"), alt: req.app.get("alt"),
   title: req.app.get("title"), date: req.app.get("date"), category: req.app.get("category")});
});

module.exports = router;
