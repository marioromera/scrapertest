var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var index = require('./routes/index');
var users = require('./routes/users');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');

const puppeteer = require('puppeteer');
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(compression());

var router = express.Router();
var imgsrc, alt;
// var baseurl = 'http://www.museoreinasofia.es';
// var reina = "http://www.museoreinasofia.es/actividades"
// var selector = '.enlace--bloque .miniatura__imagen img';
var bb = 'https://www.facebook.com/groups/browserbased/';
var titles = [];
var dates = [];
var categories = [];
var imgsrcs = [];
var alts = [];
var links = [];
var places = [];
// $('#actividades-futuras > div > div > ul > li').each((i, elm) => {
//   titles.push($(elm).find('.miniatura__titulo').text());
//   dates.push($(elm).find('.miniatura__antetitulo').text());
//   links.push(baseurl+$(elm).find('.enlace--bloque').attr('href'));
//   category = $(elm).find('.etiqueta ').text();
//   splitcategory = category.replace(/([A-Z])/g, ' $1').trim();
//   categories.push(splitcategory);
//   imgsrcs.push(baseurl + $(elm).find('img').attr('src'));
//   alts.push($(elm).find('img').attr("alt"));
//   request(baseurl+$(elm).find('.enlace--bloque').attr('href'), function (err, res, html2) {
//     var $2 = cheerio.load(html2);
//     var dateplace = $2('.titulo-pagina').find('.titulo-pagina__texto').text()
//     var place = dateplace.split('/');
//     places.push(place[1]);
//   })
// });
// });

// (async function() {
//   const instance = await phantom.create();
//   const page = await instance.createPage();
//   await page.on('onResourceRequested', function(requestData) {
//     console.info('Requesting', requestData.url);
//   });

//   const status = await page.open(bb);
//   const content = await page.property('https://www.facebook.com/groups/browserbased/');
//   content.querySelector('[role="article"]').forEach((elm, i) => {
//     console.log(elm);
//   })
//   await instance.exit();
// })();

// var articles = [];

function extractItems() {
  const extractedComments = document.querySelectorAll('.UFIPagerLink');
  const coments = [];
  for (let comment of extractedComments) {
    comment.click();
  }
  const extractedElements = document.querySelectorAll('[role="article"]');
  const items = [];
  extractedElements.forEach((article, i) => {
    const imgUrls = article.querySelectorAll('img');
    const imgs = [];
    
    imgUrls.forEach((img, i) => {
      imgs.push(img.src);
    });
    items[i] = { text: article.innerText, 'imgsrc': imgs};
  });
  // const imgUrls = document.querySelectorAll('img');
  // const imgs = [];
  // imgUrls.forEach((img, i) => {
  //   imgs.push(img.src);
  // });
  return items;
}
// function extractImages() {
//   const elements = document.querySelectorAll('img');
//   const imgs = [];
//   elements.forEach((element, i) => {
//     const { x, y, width, height } = element.getBoundingClientRect();
//     imgs[i] = { left: x, top: y, width, height, id: element.id };
//   });
//   return imgs;
// }
app.get('/', function(req, res) {
  async function scrapeInfiniteScrollItems(
    page,
    extractItems,
    itemTargetCount,
    selector,
    scrollDelay = 100
  ) {
    let items = [];
    let imgs = [];
    try {
      let previousHeight;
      while (items.length < itemTargetCount) {
        items = await page.evaluate(extractItems);
        // imgs = screenshotDOMElement(page);
        previousHeight = await page.evaluate('document.body.scrollHeight');
        await page.evaluate('window.scrollTo(0, document.body.scrollHeight)');
        await page.waitForFunction(
          `document.body.scrollHeight > ${previousHeight}`
        );
        await page.waitFor(scrollDelay);
      }
    } catch (e) {}
    return items;
  }
  // async function screenshotDOMElement(page) {
  //   try {
  //     const elements = await page.evaluate(extractImages);
  //     elements.forEach((img, i) => {
  //       page.screenshot({
  //         path: 'element' + i + '.png',
  //         clip: {
  //           x: img.left,
  //           y: img.top,
  //           width: img.width,
  //           height: img.height
  //         }
  //       });
  //     });
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  (async () => {
    // Set up browser and page.
    const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
    const page = await browser.newPage();
    page.setViewport({ width: 1280, height: 926 });

    // Navigate to the demo page.
    await page.goto('https://www.facebook.com/groups/browserbased/');

    // Scroll and extract items from the page.
    const items = await scrapeInfiniteScrollItems(page, extractItems, 10);

    res.render('index', {
      items: items
    });
    // Save extracted items to a file.
    fs.writeFileSync('./items.txt', items.join('\n') + '\n');
    console.log('finnished');

    // Close the browser.
    // await browser.close();
  })();
});
// app.get('/', function(req, res) {
//   res.render('index', {
//     titles: titles,
//     imgsrcs: imgsrcs,
//     alts: alts,
//     dates: dates,
//     categories: categories,
//     links: links,
//     places: places
//   });
// });
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
app.listen(3000);
console.log('listening on: ' + 3000);
