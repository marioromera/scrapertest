/* eslint-disable no-constant-condition */
import React, { Component } from 'react';

// import { Link } from 'react-router-dom';

import { client } from '../Client';
import Field from '../Forms/Field';
class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fields: {
        usr: '',
        pass: ''
      },
      fieldErrors: [],
      errors: [],
      showInput: false,
      isLogged: this.props.isLogged
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.state.isLogged !== nextProps.isLogged) {
      this.setState({ ...this.state, isLogged: nextProps.isLogged });
    }
  }
  onInputChange = ({ name, value, error }) => {
    const fields = this.state.fields;
    const fieldErrors = this.state.fieldErrors;

    fields[name] = value;
    fieldErrors[name] = error;

    this.setState({ fields, fieldErrors });
  };
  onFormSubmit = evt => {
    const usr = this.state.fields.usr;
    const pass = this.state.fields.pass;
    evt.preventDefault();
    client
      .login(usr, pass)
      .then(response => {
        this.setState({ ...this.state, showInput: false, isLogged: true });
      })
      .catch(error => {
        this.setState({
          ...this.state,
          errors: this.state.errors.concat(error)
        });
      });
  };
  startLogin = () => {
    if (this.state.isLogged === true) {
      this.props.updateLog(false);
      client.logout();
    } else {
      this.setState({ showInput: true, errors: [] });
    }
  };

  render() {
    const login = this.state.showInput ? (
      <div>
        <Field
          placeholder="User"
          name="usr"
          value={this.state.fields.usr}
          onChange={this.onInputChange}
        />

        <Field
          type="password"
          placeholder="Password"
          name="pass"
          value={this.state.fields.pass}
          onChange={this.onInputChange}
        />
        <span style={{ color: 'red' }}>
          {this.state.errors.length > 0 ? 'Invalid User or Pass' : null}
        </span>

        <button type="submit" onClick={this.onFormSubmit}>
          Submit
        </button>
      </div>
    ) : (
      <div>
        <a onClick={this.startLogin}>
          {this.state.isLogged ? 'Log out' : 'Log In'}
        </a>
      </div>
    );
    return <div className="logger">{login}</div>;
  }
}
export default Login;
