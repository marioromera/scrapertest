import React, { Component } from 'react';
import Login from './Login';
import { client } from '../Client';

class Header extends Component {
  state = {
    isLogged: false,
    
  };
  componentDidMount() {
    if (client.isLoggedIn()) {
      this.setState({ isLogged: true });
    }
  } 
  updateLog = (logged) =>{
    this.setState({isLogged: logged})
   }

  render() {
     
    return (
      <div className="navbar">

        <Login className="navbar nav" isLogged={this.state.isLogged ? true : false} updateLog={this.updateLog} />
      </div>
    );
  }
}

export default Header;
