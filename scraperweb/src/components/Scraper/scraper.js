import React, { Component } from 'react';
import { client } from '../Client';
import Field from '../Forms/Field';

class Scraper extends Component {
  state = {
    fields: {
      address: '',
      keywords: ''
    },
    fieldErrors: [],
    articles: []
  };
  onInputChange = ({ name, value, error }) => {
    const fields = this.state.fields;
    const fieldErrors = this.state.fieldErrors;

    fields[name] = value;
    fieldErrors[name] = error;

    this.setState({ fields, fieldErrors });
  };
  onFormSubmit = evt => {    
    const addr = this.state.fields.address;
    const keys = this.state.fields.keywords;
    evt.preventDefault();
    client.startScraping(addr, keys).then(res => {
      console.log(res);
    });
  };
  render() {
    return (
      <div className="flex-box">
        <Field
          type="text"
          name="address"
          value={this.state.fields.address}
          classNames="col-3"
          placeholder="enter an address to scrap"
          onChange={this.onInputChange}
        />
        <Field
          type="text"
          name="keywords"
          value={this.state.fieldskeywords}
          classNames="col-3"
          placeholder="enter keywords to search for"
          onChange={this.onInputChange}
        />
        <button type="submit" onClick={this.onFormSubmit}>
          Submit
        </button>
        {this.state.articles.map((article, i) => (
          <div key={i} className="cell">
            <img
              key={article.img.src}
              src={article.img.src}
              alt="article-image"
            />
            <p key={article.id}> {article.text} </p>
          </div>
        ))}
      </div>
    );
  }
}
export default Scraper;
