import React, { Component } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Scraper from './components/Scraper/scraper'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Scraper />
      </div>
    );
  }
}

export default App;
