var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var index = require('./routes/index');
var users = require('./routes/users');
var fs = require('fs');
var request = require('request');
var $ = require('jquery');
let port = 3001;
const puppeteer = require('puppeteer');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/scrappers');
const APP_SECRET = 'thisverysecretkey';
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(compression());
var db = mongoose.connection;

var router = express.Router();
var imgsrc, alt;

var titles = [];
var dates = [];
var categories = [];
var imgsrcs = [];
var alts = [];
var links = [];
var places = [];

function extractItems() {
  const extractedComments = document.querySelectorAll('.UFIPagerLink');
  const coments = [];
  for (let comment of extractedComments) {
    comment.click();
  }
  const extractedElements = document.querySelectorAll('[role="article"]');
  const items = [];
  extractedElements.forEach((article, i) => {
    const imgUrls = article.querySelectorAll('img');
    const imgs = [];

    imgUrls.forEach((img, i) => {
      imgs.push(img.src);
    });
    items[i] = {
      text: article.innerText,
      imgsrc: imgs
    };
  });

  return items;
}
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Connected to Database');
});
const extractToken = req => req.query.token;

app.get('/api/check_token', (req, res) => {
  const token = extractToken(req);
  if (token) {
    jwt.verify(token, APP_SECRET, function(err, decoded) {
      if (err) {
        res.status(401).end('Token is not valid');
      } else {
        res.status(200).end('Token is still valid');
      }
    });
  }
});
app.post('/api/login', (req, res) => {
  var response = {
    success: false
  };

  if (req.body != null) {
    db.collection('users').findOne(
      {
        usr: req.body.usr,
        pass: req.body.pass
      },
      function(err, usr) {
        if (usr) {
          let token = jwt.sign(
            {
              data: usr,
              expiresIn: '1h'
            },
            APP_SECRET
          );
          response = {
            success: true,
            token: token
          };
          res.status(200).end(JSON.stringify(response));
        } else if (err) {
          res.status(401).end('error while accessing DB');
        } else {
          res.status(401).end(JSON.stringify('No User Found'));
        }
      }
    );
  }
});
function extractItems() {
  const extractedElements = document.querySelectorAll('p');
  const items = [];
  for (let element of extractedElements) {
    items.push(element.innerText);
  }
  return items;
}
async function scrapeInfiniteScrollItems(
  page,
  extractItems,
  itemTargetCount,
  selectors,
  scrollDelay = 100
) {
  let items = [];
  try {
    let previousHeight;
     while (items.length < itemTargetCount) {
    items = await page.evaluate(searched => {
      const divs = document.querySelectorAll('div');
      const items = Array.prototype.filter.call(divs, function(element) {
        return RegExp(searched).test(element.textContent);
      });
      return items;
    }, selectors);
    previousHeight = await page.evaluate('document.body.scrollHeight');
    await page.evaluate('window.scrollTo(0, document.body.scrollHeight)');
    await page.waitForFunction(
      `document.body.scrollHeight > ${previousHeight}`
    );
    await page.waitFor(scrollDelay);
     }
    return items;
  } catch (e) {
    console.log(e);
  }
}
app.get('/scrap/:target', (req, res) => {
  let target = JSON.parse(req.params.target);
  (async () => {
    // Set up browser and page.
    const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
    const page = await browser.newPage();
    page.setViewport({
      width: 540,
      height: 420
    });

    // Navigate to the demo page.

    if (target.address.includes('http://' || 'https://')) {
      await page.goto(target.address);
    } else {
      await page.goto('http://' + target.address);
    }
    // Scroll and extract items from the page.
    const items = await scrapeInfiniteScrollItems(
      page,
      extractItems,
      10,
      target.keywords
    );
    console.log(items);

    res.status(200).end(JSON.stringify(items));
    // Save extracted items to a file.
    console.log('finnished');

    // Close the browser.
    await browser.close();
  })();
});

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
app.listen(port);
console.log('listening on: ' + port);
